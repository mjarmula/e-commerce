class Product
  class UpdateForm < ApplicationCreateForm
    attribute :id, Integer
    attribute :name, String
    attribute :category_id, Integer
    attribute :pictures, Array, default: []
    attribute :description, String
    attribute :quantity, Integer
    attribute :visible, Boolean
    attribute :slug, String

    validates :name, :category_id, :description, presence: true
    validate :unique_slug

    def self.model_name
      ActiveModel::Name.new(self, nil, 'Product')
    end

    private

    def resource
      @resource ||= Product.find(id).tap do |resource|
        resource.assign_attributes(attributes)
      end
    end

    def unique_slug
      return unless resource.slug_changed? || !Product.exists?(slug: slug)
      errors.add(:slug, 'must be unique')
    end
  end
end
