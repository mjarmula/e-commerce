class Product
  class CreateForm < ApplicationCreateForm
    attribute :name, String
    attribute :category_id, Integer
    attribute :pictures, Array, default: []
    attribute :description, String
    attribute :quantity, Integer
    attribute :visible, Boolean
    attribute :slug, String

    validates :name, :category_id, :description, presence: true
    validate :unique_slug

    def self.model_name
      ActiveModel::Name.new(self, nil, 'Product')
    end

    def slug
      @slug ||= "#{category.full_name}-#{name}".parameterize
    end

    def category
      Category.find(category_id)
    end

    private

    def resource
      @resource ||= Product.new(
        name: name,
        category_id: category_id,
        slug: slug,
        pictures: pictures,
        quantity: quantity,
        visible: visible,
        description: description
      )
    end

    def unique_slug
      return unless Product.exists?(slug: slug)
      errors.add(:slug, 'must be unique')
    end
  end
end
