class ApplicationCreateForm
  include Virtus.model
  include ActiveModel::Model

  delegate :persisted?, to: :resource

  def save
    tap do
      resource.save if valid?
    end
  end
end
