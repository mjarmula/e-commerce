class Category
  class CreateForm < ApplicationCreateForm
    extend Sluggable.new(:name)

    attribute :name, String
    attribute :parent_id, Integer, default: nil
    attribute :slug, String, default: method(:slug)

    validates :name, presence: true
    validate :unique_slug

    def self.model_name
      ActiveModel::Name.new(self, nil, 'Category')
    end

    private

    def resource
      @resource ||= Category.new(
        name: name,
        parent_id: parent_id,
        slug: slug
      )
    end

    def unique_slug
      return unless Category.exists?(name: name)
      errors.add(:name, 'must be unique')
    end
  end
end
