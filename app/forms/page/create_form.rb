class Page
  class CreateForm
    include Virtus.model
    include ActiveModel::Model
    extend Sluggable.new(:title)

    attribute :title, String
    attribute :body, String
    attribute :slug, String, default: method(:slug)

    validates :title, :body, presence: true
    validate :unique_slug

    delegate :persisted?, to: :page

    def save
      tap do
        page.save if valid?
      end
    end

    def self.model_name
      ActiveModel::Name.new(self, nil, 'Page')
    end

    private

    def page
      @page ||= Page.new(
        title: title,
        body: body,
        slug: slug
      )
    end

    def unique_slug
      return unless Page.exists?(slug: slug)
      errors.add(:title, 'must be unique')
    end
  end
end
