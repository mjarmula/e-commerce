class Page
  class UpdateForm
    include Virtus.model
    include ActiveModel::Model
    include ActiveModel::Dirty

    attribute :id, Integer
    attribute :title, String
    attribute :body, String
    attribute :slug, String
    attribute :visible, Boolean

    validates :title, :body, :slug, presence: true
    validates :visible, presence: true, allow_blank: true
    validate :unique_slug

    delegate :persisted?, to: :page

    def save
      tap do
        page.save if valid?
      end
    end

    def self.model_name
      ActiveModel::Name.new(self, nil, 'Page')
    end

    private

    def page
      @page ||= Page.find(id).tap do |page|
        page.assign_attributes(attributes)
      end
    end

    def unique_slug
      return if !Page.exists?(slug: slug) || !page.slug_changed?
      errors.add(:slug, 'must be unique')
    end
  end
end
