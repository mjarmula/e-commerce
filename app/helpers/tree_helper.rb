module TreeHelper
  def render_tree_view(nodes, response = '')
    nodes.each do |node|
      response += render_tree_item(node)
    end
    response.html_safe
  end

  def render_tree_item(node)
    render partial: 'admin/shared/tree_item', locals: { node: node }
  end

  def tree_select_box(nodes, select_options = [])
    select_options.tap do
      nodes.each do |node|
        select_options.push([node_name_with_level(node), node[:id]])
        unless node[:children].empty?
          tree_select_box(node[:children], select_options)
        end
      end
    end
  end

  private

  def node_name_with_level(node)
    '-' * (node[:level] + 1) + ' ' + node[:name]
  end
end
