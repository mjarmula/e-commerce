class Category
  class TreeSerializer
    attr_reader :categories, :structure
    private :categories, :structure

    def initialize(categories)
      @categories = categories
      @structure = []
    end

    def self.serialize(categories)
      new(categories).serialize
    end

    def serialize
      structure.tap do |structure|
        categories.each { |category| structure.push category_hash(category) }
      end
    end

    private

    def category_hash(category)
      {
        name: category.name,
        id: category.id,
        children: children_categories(category),
        level: category.level
      }
    end

    def children_categories(category)
      if category.children.empty?
        []
      else
        category.children.map(&method(:nested_hash))
      end
    end

    def nested_hash(child_category)
      category_hash(child_category)
    end
  end
end
