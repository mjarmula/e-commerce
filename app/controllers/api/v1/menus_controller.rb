module Api
  module V1
    class MenusController < ApplicationController
      def show
        @menu = Menu.fetch(name: params[:name])
      end
    end
  end
end
