module Api
  module V1
    class PagesController < ApplicationController
      before_action :set_page, only: %i[show]

      def show; end

      private

      def set_page
        @page = Page.find_by(slug: params[:slug])
      end
    end
  end
end
