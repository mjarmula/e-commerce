module Admin
  class ProductsController < ApplicationController
    before_action :set_category, only: %i[edit new create update]
    before_action :set_product, only: %i[edit update]

    def new
      @product_form = Product.new
    end

    def create
      @product_form = Product::CreateForm.new(create_params)
      response = Product::Create.call(form: @product_form)

      if response.success?
        flash[:success] = response.success_message
        redirect_to admin_root_path
      else
        flash[:error] = response.error_message
        render :new
      end
    end

    def edit; end

    def update
      @product = Product::UpdateForm.new(update_params)
      response = Product::Update.call(form: @product)

      if response.success?
        flash[:success] = response.success_message
        redirect_to admin_root_path, success: response.success_message
      else
        flash[:error] = response.error_message
        render :edit
      end
    end

    private

    def create_params
      params.require(:product).permit(
        :name, :description, :quantity, :category_id, :visible, pictures: []
      ).merge(category_id: params[:category_id])
    end

    def update_params
      create_params.merge(slug: params[:product][:slug],
                          id: params[:product][:id])
    end

    def set_category
      @category = Category.find(params[:category_id])
    end

    def set_product
      @product = Product.find(params[:id])
    end
  end
end
