module Admin
  class CategoriesController < ApplicationController
    helper CategoriesPresenter

    def index; end

    def show; end

    def update
      category = Category.find(params[:id])
      category.update_attributes!(update_params)
      head :ok
    end

    def new
      @category_form = Category.new
    end

    def create
      @category_form = Category::CreateForm.new(create_params)
      response = Category::Create.call(form: @category_form)

      if response.success?
        flash[:success] = response.success_message
        redirect_to admin_categories_path
      else
        flash[:error] = response.error_message
        render :new
      end
    end

    def destroy
      category = Category.find(params[:id])
      response = Category::Destroy.call(resource: category)
      flash[:success] = response.success_message
      redirect_to admin_categories_path
    end

    private

    def update_params
      params.require(:category).permit(:parent_id, :position)
    end

    def create_params
      params.require(:category).permit(:name, :parent_id)
    end
  end
end
