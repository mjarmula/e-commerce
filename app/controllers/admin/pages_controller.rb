module Admin
  class PagesController < ApplicationController
    def index
      @pages = Page.all
    end

    def new
      @page_form = Page.new
    end

    def create
      @page_form = Page::CreateForm.new(create_params)
      response = Page::Create.call(form: @page_form)

      if response.success?
        flash[:success] = response.success_message
        redirect_to admin_root_path
      else
        flash[:error] = response.error_message
        render :new
      end
    end

    def edit
      @page = Page.find(params[:id])
    end

    def update
      @page = Page::UpdateForm.new(update_params)
      response = Page::Update.call(form: @page)

      if response.success?
        flash[:success] = response.success_message
        redirect_to admin_root_path, success: response.success_message
      else
        flash[:error] = response.error_message
        render :edit
      end
    end

    private

    def create_params
      params.require(:page).permit(:title, :body)
    end

    def update_params
      params.require(:page).permit(:title, :body, :slug, :visible, :id)
    end
  end
end
