class Product < ApplicationRecord
  belongs_to :category

  mount_uploaders :pictures, PictureUploader
  serialize :pictures, JSON

  def pictures?
    pictures.any?
  end
end
