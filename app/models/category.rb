class Category < ApplicationRecord
  extend ActsAsTree::TreeWalker
  extend ActsAsTree::TreeView

  acts_as_list scope: :parent_id
  acts_as_tree order: 'position'

  has_many :children, class_name: 'Category', foreign_key: 'parent_id',
                      dependent: :destroy
  has_many :products, dependent: :destroy

  scope :root_categories, -> { where(parent_id: nil).order(:position) }

  def parents(node = self, node_parents = [])
    node_parents.tap do
      unless node.parent.nil?
        node_parents.push(node.parent)
        parents(node.parent, node_parents)
      end
    end
  end

  def full_name
    "#{name} - #{parents.map(&:name).join('-')}".parameterize
  end
end
