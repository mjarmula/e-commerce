class Sluggable < Module
  extend ActiveSupport::Concern

  def initialize(target)
    @target = target
  end

  def extended(_)
    target = @target
    define_method(:slug) do |resource, _|
      return unless resource[target]
      resource[target].parameterize
    end
  end
end
