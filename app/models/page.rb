class Page < ApplicationRecord
  acts_as_list

  scope :visible, -> { where(visible: true) }
end
