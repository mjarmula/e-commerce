class ApplicationDestroy
  attr_reader :resource
  private :resource

  def initialize(resource)
    @resource = resource
  end

  def self.call(resource:)
    new(resource).destroy
  end

  def destroy
    resource.destroy
    response
  end
end
