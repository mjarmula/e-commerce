class Menu
  class TopMenu
    def initialize; end

    def self.to_api
      new.to_api
    end

    def to_api
      Page.visible.order(position: :asc).select(:title, :position, :slug)
    end
  end
end
