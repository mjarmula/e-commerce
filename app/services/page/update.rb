class Page
  class Update
    attr_reader :form
    private :form

    def initialize(form)
      @form = form
    end

    def self.call(form:)
      new(form).save
    end

    def save
      form.save
      response
    end

    private

    def response
      Page::UpdateResponse.new(form)
    end
  end
end
