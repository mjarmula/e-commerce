class Menu
  attr_reader :name
  private :name

  def initialize(name)
    @name = name.to_sym
  end

  def self.fetch(name:)
    new(name).fetch.to_api
  end

  def fetch
    defined_menus.fetch(name)
  end

  private

  def defined_menus
    {
      top: TopMenu
    }
  end
end
