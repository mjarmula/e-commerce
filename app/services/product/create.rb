class Product
  class Create
    attr_reader :form
    private :form

    def initialize(form)
      @form = form
    end

    def self.call(form:)
      new(form).save
    end

    def save
      form.save
      response
    end

    private

    def response
      Product::CreateResponse.new(form)
    end
  end
end
