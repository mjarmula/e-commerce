class Category
  class Destroy < ApplicationDestroy
    private

    def response
      Category::DestroyResponse.new
    end
  end
end
