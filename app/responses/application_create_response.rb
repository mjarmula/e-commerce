class ApplicationCreateResponse
  attr_reader :form

  def initialize(form)
    @form = form
  end

  def success?
    form.persisted?
  end

  def error_message
    form.errors.full_messages.to_sentence
  end
end
