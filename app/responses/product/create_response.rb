class Product
  class CreateResponse < ApplicationCreateResponse
    def success_message
      'Product was created successfully'
    end
  end
end
