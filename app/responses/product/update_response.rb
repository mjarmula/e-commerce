class Product
  class UpdateResponse < ApplicationCreateResponse
    def success_message
      'Product was updated successfully'
    end
  end
end
