class Page
  class CreateResponse
    attr_reader :page_form

    def initialize(page_form)
      @page_form = page_form
    end

    def success?
      page_form.persisted?
    end

    def error_message
      page_form.errors.full_messages.to_sentence
    end

    def success_message
      'Page was created successfully'
    end
  end
end
