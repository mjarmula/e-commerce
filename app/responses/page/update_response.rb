class Page
  class UpdateResponse
    attr_reader :page_form

    def initialize(page_form)
      @page_form = page_form
    end

    def success?
      page_form.valid?
    end

    def error_message
      page_form.errors.full_messages.to_sentence
    end

    def success_message
      'Page was updated successfully'
    end
  end
end
