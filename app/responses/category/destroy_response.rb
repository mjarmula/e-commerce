class Category
  class DestroyResponse
    def success_message
      I18n.t('categories.destroyed')
    end
  end
end
