class Category
  class CreateResponse < ApplicationCreateResponse
    def success_message
      'Category was created successfully'
    end
  end
end
