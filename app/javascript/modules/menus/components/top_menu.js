import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchMenu } from '../actions/index';
import { Navbar } from 'react-materialize';
import { withRouter } from 'react-router';
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";

class TopMenu extends Component {
  componentDidMount() {
    this.props.fetchMenu('top');
  }

  renderMenuItem(menuData) {
    return(
      <li key={menuData.slug}>
        <NavLink to={`/pages/${menuData.slug}`}>
        {menuData.title}
        </NavLink>
      </li>
    );
  }

  navigate(path) {
    this.props.history.push(path);
  }

  render() {
    return(
      <Navbar>
        {this.props.topMenu.map(this.renderMenuItem.bind(this))}
      </Navbar>
    );
  }
}

function mapStateToProps({ topMenu }) {
  return { topMenu };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchMenu }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(TopMenu));
