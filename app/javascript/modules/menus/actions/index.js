import axios from 'axios';
const ROOT_URL = '/api/v1/menus';

export const FETCH_MENU = 'FETCH_MENU';

export function fetchMenu(menu) {
  var url = `${ROOT_URL}/${menu}.json`;
  var request = axios.get(url);

  return {
    type: FETCH_MENU,
    payload: request
  }
}
