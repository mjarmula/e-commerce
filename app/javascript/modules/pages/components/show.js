import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchPage } from '../actions/index';

class Show extends Component {
  componentDidMount() {
    this.props.fetchPage(this.props.match.params.slug);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.match.params.slug !== this.props.match.params.slug) {
      this.props.fetchPage(nextProps.match.params.slug);
    }
  }

  constructor(props) {
    super(props);
  }

  render() {
    if(!this.props.page)
      return <div>Loading...</div>;
    return <div dangerouslySetInnerHTML={{ __html: this.props.page.body }} />;
  }
}

function mapStateToProps({ page }) {
  return { page };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchPage }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Show);
