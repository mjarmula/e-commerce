import { FETCH_PAGE } from '../actions/index';

const defaultPage = {title:'', body:''}

export default function (state = defaultPage, action) {
  switch(action.type) {
    case FETCH_PAGE : {
      return action.payload.data;
    }

    default:
      return state;
  }
}
