import axios from 'axios';
const ROOT_URL = '/api/v1/pages';

export const FETCH_PAGE = 'FETCH_PAGE';

export function fetchPage(page = 'home') {
  var url = `${ROOT_URL}/${page}.json`;
  var request = axios.get(url);

  return {
    type: FETCH_PAGE,
    payload: request
  }
}
