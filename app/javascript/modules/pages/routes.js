export default const function() {
  return (
    <Switch>
      <Route exact path='/pages' component={Pages.Show} page='home'/>
      <Route path='/pages/:name' component={Pages.Showlayer}/>
    </Switch>
  );
}
