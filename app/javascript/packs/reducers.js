import { combineReducers } from 'redux';
import PageReducer from '../modules/pages/reducers/page';
import TopMenuReducer from '../modules/menus/reducers/top_menu';

const rootReducer = combineReducers({
  topMenu: TopMenuReducer,
  page: PageReducer
});

export default rootReducer;
