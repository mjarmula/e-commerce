module ProductsPresenter
  def product
    @product ||= Product.find(params[:id])
  end

  def category
    @category ||= Category.find(params[:category_id])
  end
end
