module CategoriesPresenter
  def category_tree
    Category::TreeSerializer.serialize(Category.root_categories)
  end

  def categories
    Category::TreeSerializer.serialize(Category.root_categories)
  end

  def category
    @category ||= Category.find(params[:id])
  end
end
