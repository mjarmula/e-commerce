// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery3
//= require jquery_ujs
//= require jquery-ui
//= require turbolinks
//= require_directory .
/*WYSWIG*/
//= require froala_editor.min
//= require plugins/align.min
//= require plugins/colors.min
//= require plugins/entities.min
//= require plugins/font_family.min
//= require plugins/font_size.min
//= require plugins/help.min
//= require plugins/line_breaker.min
//= require plugins/link.min
//= require plugins/lists.min
//= require plugins/paragraph_format.min
//= require plugins/paragraph_style.min
//= require plugins/special_characters.min
//= require plugins/url.min
//= require plugins/code_view.min
