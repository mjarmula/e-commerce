Rails.application.routes.draw do
  root 'react#home'

  namespace :api do
    namespace :v1 do
      resources :menus, only: %i[show], param: :name
      resources :pages, only: %i[show], param: :slug
    end
  end

  namespace :admin do
    resources :pages
    resources :categories do
      resources :products, except: %i[index]
    end
    root 'pages#index'
  end

  get '*path', to: 'react#home'
end
