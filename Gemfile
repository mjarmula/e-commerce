source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '~> 5.1.6'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.7'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'webpacker'
gem 'slim-rails'
gem 'bootstrap-sass', '~> 3.3.6'
gem 'acts_as_tree', '~> 2.7', '>= 2.7.1'
gem 'jquery-ui-rails', '~> 6.0', '>= 6.0.1'
gem 'carrierwave', '~> 1.2', '>= 1.2.2'
gem 'mini_magick', '~> 4.8'

gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'acts_as_list', '~> 0.9.11'
gem 'devise', '~> 4.4', '>= 4.4.3'
gem 'wysiwyg-rails', '~> 2.7', '>= 2.7.6'
gem 'jquery-rails', '~> 4.3', '>= 4.3.1'
gem 'virtus', '~> 1.0', '>= 1.0.5'

group :development, :test do
  gem 'pry'
  gem 'rspec-rails', '~> 3.0'
  gem 'capybara'
  gem 'database_cleaner'
  gem 'cucumber-rails', require: false
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'faker', :git => 'https://github.com/stympy/faker.git', :branch => 'master'
end

group :test do
  gem 'shoulda-matchers', '~> 3.1'
  gem 'factory_girl_rails'
  gem 'rails-controller-testing', '~> 1.0', '>= 1.0.2'
  gem 'json_matchers', '~> 0.7.3'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
