class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name, null: false
      t.integer :quantity, null: false, default: 0
      t.text :description, null: false
      t.string :slug, null: false
      t.boolean :visible, null: false, default: false
      t.string :pictures, null: true
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
