class CreateCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :categories do |t|
      t.string :name
      t.integer :position
      t.integer :parent_id
      t.string :slug, null: false

      t.timestamps
      t.index :parent_id, unique: false
    end
  end
end
