class CreatePages < ActiveRecord::Migration[5.1]
  def change
    create_table :pages do |t|
      t.string :title, null: false
      t.string :slug, null: false
      t.text :body, null: false
      t.boolean :visible, null: false, default: false
      t.integer :position

      t.timestamps
      t.index :slug, unique: true
    end
  end
end
