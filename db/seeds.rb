require 'faker'

Page.create(title: 'Home Page', body: 'Sample home page', position: 1, visible: true, slug: 'home'.parameterize)
Page.create(title: 'About', body: 'Sample about page', position: 2, visible: true, slug: 'about'.parameterize)

10.times do |number|
  Category.create(name: Faker::Commerce.material,
                  position: number,
                  slug: "category-#{number}")
end

10.times do |number|
  Product.create(name: Faker::Commerce.product_name,
                 description: Faker::Lorem.paragraphs.join(' '),
                 quantity: Random.rand(10),
                 category_id: Random.rand(1..10),
                 visible: number % 2,
                 slug: "product-#{number}")
end
