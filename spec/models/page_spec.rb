require 'rails_helper'

RSpec.describe Page, type: :model do
  describe '.visible' do
    let(:visible_page) { create(:pages, visible: true) }
    let(:invisible_page) { create(:pages, visible: true) }

    subject { described_class.visible }

    it 'returns visible pages' do
      is_expected.to eq([visible_page])
    end
  end
end
