FactoryGirl.define do
  factory :pages, class: Page do
    title 'Title'
    body 'Body'
    slug 'slug'
    position 0
    visible false
  end
end
