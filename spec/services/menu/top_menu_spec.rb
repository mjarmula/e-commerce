require 'rails_helper'

RSpec.describe Menu::TopMenu do
  describe '.to_api' do
    it 'returns visible pages' do
      expect(Page).to receive_message_chain('visible.order.select')

      described_class.to_api
    end
  end
end
