require 'rails_helper'

RSpec.describe Page::Create do
  before do
    allow(form_object).to receive(:save)
    allow_any_instance_of(described_class).to receive(:form)
      .and_return(form_object)
    allow(form_object).to receive(:valid?)
  end

  describe '.call' do
    subject { described_class.call(form: form_object) }

    context 'when page was persited' do
      let(:form_object) { double('Page::Create::Form', persisted?: true) }

      it 'returns successful response' do
        is_expected.to be_a(Page::CreateResponse)
        expect(subject.success?).to be_truthy
      end
    end

    context 'when page was not persited' do
      let(:form_object) { double('Page::Create::Form', persisted?: false) }

      it 'returns unsuccessful response' do
        is_expected.to be_a(Page::CreateResponse)
        expect(subject.success?).to be_falsy
      end
    end
  end
end
