require 'rails_helper'

RSpec.describe Page::Update do
  before do
    allow(form_object).to receive(:save)
    allow_any_instance_of(described_class).to receive(:form)
      .and_return(form_object)
  end

  describe '.call' do
    subject { described_class.call(form: form_object) }

    context 'when page was persisted' do
      let(:form_object) { double('Page::UpdateForm', valid?: true) }

      it 'returns successful response' do
        is_expected.to be_a(Page::UpdateResponse)
        expect(subject.success?).to be_truthy
      end
    end

    context 'when page was not persisted' do
      let(:form_object) { double('Page::UpdateForm', valid?: false) }

      it 'returns unsuccessful response' do
        is_expected.to be_a(Page::UpdateResponse)
        expect(subject.success?).to be_falsy
      end
    end
  end
end
