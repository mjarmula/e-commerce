require 'rails_helper'

RSpec.describe Product::Create do
  before do
    allow(form_object).to receive(:save)
    allow_any_instance_of(described_class).to receive(:form)
      .and_return(form_object)
    allow(form_object).to receive(:valid?)
  end

  describe '.call' do
    subject { described_class.call(form: form_object) }

    context 'when product was persited' do
      let(:form_object) { double(Product::CreateForm, persisted?: true) }

      it 'returns successful response' do
        is_expected.to be_a(Product::CreateResponse)
        expect(subject.success?).to be_truthy
      end
    end

    context 'when product was not persited' do
      let(:form_object) { double(Product::CreateForm, persisted?: false) }

      it 'returns unsuccessful response' do
        is_expected.to be_a(Product::CreateResponse)
        expect(subject.success?).to be_falsy
      end
    end
  end
end
