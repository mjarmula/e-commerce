require 'rails_helper'

RSpec.describe Product::CreateResponse do
  let(:product_form) { double(Product::CreateForm) }
  let(:success_message) { 'Product was created successfully' }

  subject { described_class.new(product_form) }

  describe '#success_message' do
    it 'returns correct message' do
      expect(subject.success_message).to eq(success_message)
    end
  end

  describe '#success?' do
    context 'when the product form is valid' do
      let(:product_form) { double(Product::CreateForm, persisted?: true) }

      it 'returns true' do
        expect(subject.success?).to be_truthy
      end
    end

    context 'when the product form is invalid' do
      let(:product_form) { double(Product::CreateForm, persisted?: false) }

      it 'returns true' do
        expect(subject.success?).to be_falsy
      end
    end
  end
end
