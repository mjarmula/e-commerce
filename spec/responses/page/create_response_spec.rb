require 'rails_helper'

RSpec.describe Page::CreateResponse do
  let(:page_form) { double('Page::CreateForm') }
  let(:success_message) { 'Page was created successfully' }

  subject { described_class.new(page_form) }

  describe '#success_message' do
    it 'returns correct message' do
      expect(subject.success_message).to eq(success_message)
    end
  end

  describe '#success?' do
    context 'when the page form is valid' do
      let(:page_form) { double('Page::CreateForm', persisted?: true) }

      it 'returns true' do
        expect(subject.success?).to be_truthy
      end
    end

    context 'when the page form is invalid' do
      let(:page_form) { double('Page::CreateForm', persisted?: false) }

      it 'returns true' do
        expect(subject.success?).to be_falsy
      end
    end
  end
end
