require 'rails_helper'

RSpec.describe Page::UpdateForm do
  describe '#save' do
    subject { described_class.new(params).save }
    let(:page) { create(:pages) }

    context 'when params are valid' do
      let(:params) do
        {
          'id' => page.id,
          'title' => 'New title',
          'body' => 'New body',
          'slug' => 'New slug',
          'visible' => true
        }
      end

      let(:updated_attributes) do
        Page.find(page.id).attributes.slice(
          'id', 'title', 'body', 'slug', 'visible'
        )
      end

      it 'returns info about the state' do
        expect(subject.valid?).to be_truthy
      end

      it 'updates record' do
        subject
        expect(updated_attributes).to eql(params)
      end
    end

    context 'when params are invalid' do
      let(:params) { { title: 'New title' } }

      it 'returns validation info' do
        expect(subject.valid?).to be_falsy
      end
    end

    context 'when params are valid but slug is already taken' do
      let!(:page_with_taken_slug) { create(:pages, slug: 'taken') }
      let(:params) do
        {
          id: page.id,
          title: 'New title',
          body: 'New body',
          slug: 'taken',
          visible: true
        }
      end

      let(:tested_attributes) do
        Page.find(page.id).attributes.except('created_at', 'updated_at')
      end

      let(:expected_attributes) do
        page.attributes.except('created_at', 'updated_at')
      end

      it 'returns invalid form' do
        expect(subject.valid?).to be_falsy
      end

      it 'doesnt update attributes' do
        expect(tested_attributes).to eq(expected_attributes)
      end
    end
  end
end
