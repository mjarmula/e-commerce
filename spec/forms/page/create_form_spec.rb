require 'rails_helper'

RSpec.describe Page::CreateForm do
  describe '#save' do
    subject { described_class.new(params).save }

    context 'when params are valid' do
      let(:params) { { title: 'Test title', body: 'Test body' } }

      it 'saves page' do
        expect { subject }.to change(Page, :count).by(1)
      end

      it 'returns info about resource persistence' do
        expect(subject.persisted?).to be_truthy
      end
    end

    context 'when params are invalid' do
      let(:params) { { title: 'Test title' } }

      it 'doesnt save page' do
        expect { subject }.to_not change(Page, :count)
      end

      it 'returns info about resource persistence' do
        expect(subject.persisted?).to be_falsy
      end
    end
  end
end
