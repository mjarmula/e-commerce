require 'rails_helper'

RSpec.describe 'GET /api/v1/menu/:name', type: :request do
  describe '#show' do
    it 'returns status 200' do
      get api_v1_menu_path name: 'top', format: 'json'

      expect(response).to be_success
      expect(response).to match_json_schema('menu')
    end
  end
end
